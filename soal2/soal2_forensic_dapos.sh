#!/bin/bash
mkdir -p forensic_log_website_daffainfo_log

cat log_website_daffainfo.log | awk -F '\t' 'NR>1 && NF=1' | awk -F\" '{print substr($4,1,14)}' | uniq -c | awk '{sum+=$1;row++} END {print "Rata-rata serangan adalah sebanyak",sum/row,"requests per jam"}' > ./forensic_log_website_daffainfo_log/ratarata.txt

cat log_website_daffainfo.log | awk -F '\t' 'NR>1 && NF=1' | awk -F\" '{print $2}' | uniq -c | sort -rn | awk -F '\t' 'NR==1 && NF=1' | awk '{print "IP yang paling banyak mengakses server adalah:",$2,"sebanyak",$1,"requests\n"}' > ./forensic_log_website_daffainfo_log/result.txt

cat log_website_daffainfo.log | awk '/curl/ {all++} END {print "Ada",all,"requests yang menggunakan curl sebagai user-agent\n"}' >> ./forensic_log_website_daffainfo_log/result.txt

cat log_website_daffainfo.log | awk -F\" '/curl/ {print $2,"Jam",substr($4,13,2),"pagi"}' >> ./forensic_log_website_daffainfo_log/result.txt
