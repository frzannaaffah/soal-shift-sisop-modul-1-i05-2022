# Laporan Penjelasan Soal Shift Modul 1 
Sistem Operasi 2022 
## IUP 

#### Asisten : Muhammad Raihan
#### Anggota : 

<ul>
<li>
Farzana Afifah Razak - 5025201130 </li>
<li>Raihan Farid - 5025201141 </li>
<li>Rangga Aulia Pradana - 5025201154 </li>
</ul>



### Daftar isi
[TOC]



## Soal 1 
Penjelasan code soal nomor 1 :
```
#!/bin/bash

echo "Register Account"
echo "================"
while true
do
    echo -n "Username: "
    read Username
    echo -n "Password: "
    read -s Password
    PassLen=${#Password}
    list=./users/user.txt
    echo ""
```

Pada code diatas, akan muncul output meminta `username` `password`, dan meng-initialize `panjang password` berdasarkan variabel `password`. Dan menetapkan list pengguna yang akan disimpan pada folder `./users/user.txt`, lalu memulai sebuah while loop.

```
 if [ -z "$Username" ] || [ -z "$Password" ]
    then
        echo "Username and Password cannot be empty"
        continue
    elif [ $Username == $Password ]
    then
        echo "Password and Password cannot same"
        continue
    elif [[ "$Password" =~ [^a-zA-Z0-9] ]]
    then 
        echo "Password must alphanumeric"
        continue
    elif [ $PassLen -lt 8 ]
    then
        echo "Password minimal 8 characters"
        continue
    elif ! [[ "$Password" =~ [[:upper:]] && "$Password" =~ [[:lower:]] ]] 
    then
        echo "No upper or lower case detected!"
        continue
    fi
```
While loop yang dimaksud bertujuan untuk memeriksa setiap input `Username` dan `password` apakah sudah sesuai dengan ketentuan atau belum.

```
    case `grep -e "^$Username:" "$list" &>/dev/null; echo $?` in
    0)
        echo "Username already exists"
        echo "$(date +'%m/%d/%y') $(date +'%H:%M:%S') REGISTER: ERROR User already exists" >> log.txt
        continue
        ;;
    1)
        echo "Registering you right away..."
        echo "$Username:$Password" >> ./users/user.txt
        echo "$(date +'%m/%d/%y') $(date +'%H:%M:%S') REGISTER:INFO User $Username registered successfully" >> ./log.txt
        break
        ;;
    *)
        echo "Creating new file for you..."
        $(mkdir users 2>/dev/null)
        echo "$Username:$Password" >> ./users/user.txt
        break
        ;;
    esac
done
```
Setelah itu, memeriksa opsi login, apakah username yang dimasukkan sudah terdaftar dalam list atau belum, dan melakukan output sesuai dengan kondisi code tersebut. Apabila belum ada input yang terdaftar, akan melakukan output bahwa register telah selesai dan menambahkan user tersebut di dalam file pada lokasi `./users/user.txt`.

### Permasalahan soal nomor 1 :
Pada soal ini, praktikan tidak dapat membuat script yang bernama `main.sh`, dikarenakan tidak mampu dalam menyelesaikan soal 1.D.


## Soal 2 
Penjelasan code soal nomor 2 :

![code.png](https://drive.google.com/uc?export=view&id=1duEAHfRTBqecFuPvrrFRJ9P2svb3isgH)


2.A
```
    #!/bin/bash
    mkdir -p forensic_log_website_daffainfo_log

```

Pada bagian ini `mkdir` berfungsi untuk membuat folder baru dan `-p` supaya dia tidak memunculkan error apabila foldernya sudah ada sebelumnya

2.B

```
    cat log_website_daffainfo.log | awk 'BEGIN{sum=-1}{sum++;}END{print "Rata-rata serangan adalah sebanyak "sum/12 " per jam"}' > ./forensic_log_website_daffainfo_log/ratarata.txt

```

- `BEGIN` digunakan untuk memulai menghitung baris
- Lalu `{sum-1}` dikarenakan baris awal tidak dihitung sehingga -1
- `{sum++}`untuk increment pada setiap baris yang dihitung 
- Setelah dihitung dilanjutkan dengan mencetak `sum/12` karena request berhenti jam 12 jadi dibagi dengan 12

![ratrata.png](https://drive.google.com/uc?export=view&id=1bhTjEWURK9au8tNK7ckUeK3-NqmyqWO_)

2.C

```
    cat log_website_daffainfo.log | awk -F '\t' 'NR>1 && NF=1' | awk -F\" '{print $2}' | uniq -c | sort -rn | awk -F '\t' 'NR==1 && NF=1' | awk '{print "IP yang paling banyak mengakses server adalah:",$2,"sebanyak",$1,"requests\n"}' > ./forensic_log_website_daffainfo_log/result.txt

```

melakukan split untuk mendapatkan alamat IPnya, lalu semua IP dibandingkan satu dengan lainnya menggunakan for loop. `If` nya digunakan agar nanti jika ada jumlah address yang lebih banyak akan diganti ke IP address yang lebih banyak. hingga akhir di print jumlah IPnya dan alamat IPnya


2.D

```
    cat log_website_daffainfo.log | awk '{split ($1,a,":");b[a[1]]++;max=0} END {for (i in b) {if (b[i] > max) {max=b[i]; ip=i}} print "IP yang paling banyak mengakses server adalah:",ip,"sebanyak",max,"requests\n"}' > ./forensic_log_website_daffainfo_log/result.txt

```

`/curl/{all++}` berfungsi untuk mencari kata curl di dalam data dan dihitung total kata tersebut

Setelah selesai dimasukkan ke folder forensic_log_website_daffainfo_log pada file result.txt

2.E

```
    cat log_website_daffainfo.log | awk -F\" '/2022:02/ {print $2,"Jam 2 pagi"}' | uniq -c | awk ' {print $2}' >> ./forensic_log_website_daffainfo_log/result.txt
    
```

`'/2022:02/ {print $2,"Jam 2 pagi"}'` untuk mencari dan memprint alamat IP pada tanggal 22 pada jam 2 pagi

![result.png](https://drive.google.com/uc?export=view&id=1rT91zZHA8RTlEiDrg6e4Ln588s7H327q)

## Soal 3 
Penjalasan code soal  nomor 3 :

```
    #!/bin/bash 

    for num in 1
    do
        echo "$(date '+%Y-%m-%d %H:%M:%S')"
```

Pada bagian ini, saya memulai sebuah for-looping yang bertujuan untuk menyimpan tahun,bulan,tanggal, jam,menit, dan detik kapan data ini di ambil 

```
        echo "$(free -m | grep Mem: | sed 's/Mem://g') $(free -m | grep Swap: | sed 's/Swap://g') $(du -sh /home/parid)" > metrics_$(date '+%Y%m%d%H%M%S').log

        sleep 1
    done
```

- Setelah tanggal tersebut ditampilkan, pemanggilan selanjutnya yang dimulai dengan pemanggilan memory size `free -m` lalu di _pipeline_ dengan memanggil `grep Mem:`, dan di pipe kembali dengan menambahkan `| sed 's/Mem://g'`, yang bertujuan untuk menghilangkan `Mem:` pada setiap output data memory. 


- Panggilan kedua yang bertujuan untuk menyimpan data `Swap_memory`, saya menggunakan code yang sama dengan `main memory`


- Pada panggilan ketiga, saya memanggil `du -sh` untuk menyimpan resource disk pada format `/home/user(parid)`


- Lalu melakukan output dari data tersebut pada file baru yang bernama `metrics_$(date '+%Y%m%d%H%M%S').log`, yang bertujuan agar setiap log yang di output menyimpan waktu kapan file tersebut dijalankan.


### Permasalahan soal nomor 3 : 
Pada soal nomor 3, tidak dapat menampilkan aggregate_minute_to_hourly.sh, dikarenakan bingung saat menggunakan file-file yang bertujuan untuk di input ke dalam script tersebut


