#!/bin/bash

echo "Register Account"
echo "================"
while true
do
    echo -n "Username: "
    read Username
    echo -n "Password: "
    read -s Password
    PassLen=${#Password}
    list=./users/user.txt
    echo ""
    if [ -z "$Username" ] || [ -z "$Password" ]
    then
        echo "Username and Password cannot be empty"
        continue
    elif [ $Username == $Password ]
    then
        echo "Password and Password cannot same"
        continue
    elif [[ "$Password" =~ [^a-zA-Z0-9] ]]
    then 
        echo "Password must alphanumeric"
        continue
    elif [ $PassLen -lt 8 ]
    then
        echo "Password minimal 8 characters"
        continue
    elif ! [[ "$Password" =~ [[:upper:]] && "$Password" =~ [[:lower:]] ]] 
    then
        echo "No upper or lower case detected!"
        continue
    fi
    case `grep -e "^$Username:" "$list" &>/dev/null; echo $?` in
    0)
        echo "Username already exists"
        echo "$(date +'%m/%d/%y') $(date +'%H:%M:%S') REGISTER: ERROR User already exists" >> log.txt
        continue
        ;;
    1)
        echo "Registering you right away..."
        echo "$Username:$Password" >> ./users/user.txt
        echo "$(date +'%m/%d/%y') $(date +'%H:%M:%S') REGISTER:INFO User $Username registered successfully" >> ./log.txt
        break
        ;;
    *)
        echo "Creating new file for you..."
        $(mkdir users 2>/dev/null)
        echo "$Username:$Password" >> ./users/user.txt
        break
        ;;
    esac
done
